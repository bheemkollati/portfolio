import React from 'react'
import './intro.scss';
import { init } from 'ityped'
import {useEffect , useRef} from 'react';

function Intro() {

    const textRef = useRef();

    useEffect(()=>{
        init(textRef.current,{
            showCursor:true,
            backDelay:1000,
            backSpeed:50,
            strings:["Developer","Designer","Coder"]
        })
    },[]);
    return (
        <div className="intro" id="intro">
            <div className="left">
                <div className="imgContainer">
                    <img src="/assets/man.png" alt="" />
                </div>
            </div>
            <div className="right">
                <div className="wrapper">
                    <h2>Hi There, I'am</h2>
                    <h1>Bheem Kollati</h1>
                    <h3>Freelance <span ref={textRef}>Designer</span></h3>
                </div>
                <a href="#portfolio">
                    <img src="/assets/down.png" alt=""/>
                </a>
            </div>
        </div>
    )
}

export default Intro
